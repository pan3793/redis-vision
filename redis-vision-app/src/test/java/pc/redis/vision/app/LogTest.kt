package pc.redis.vision.app

import org.apache.commons.logging.LogFactory
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler

class LogTest {
    companion object {
        init {
            SLF4JBridgeHandler.removeHandlersForRootLogger()
            SLF4JBridgeHandler.install()
        }
    }

    private val log: Logger = LoggerFactory.getLogger(LogTest::class.java)
    private val jdkLog: java.util.logging.Logger = java.util.logging.Logger.getLogger(LogTest::class.java.name)
    private val commonsLog: org.apache.commons.logging.Log = LogFactory.getLog(LogTest::class.java)
    private val log4jLog: org.apache.log4j.Logger = org.apache.log4j.LogManager.getLogger(LogTest::class.java)

    @Test
    fun testLog() {
        log.debug("this is a debug message")
        log.info("this is an info message")
        log.warn("this is a warn message")
        log.error("this is an error message")

        jdkLog.info("this an info message from jdk log")
        commonsLog.info("this an info message from commons log")
        log4jLog.info("this an info message from log4j log")
    }
}