package pc.redis.vision.app.util

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

object DateUtil {
    val DEFAULT_DATE_FORMAT_PATTERN = "yyyy-MM-dd"
    val DEFAULT_TIME_FORMAT_PATTERN = "HH:mm:ss"
    val MS_TIME_FORMAT_PATTERN = "HH:mm:ss.SSS"
    val DEFAULT_DATETIME_FORMAT_PATTERN = "$DEFAULT_DATE_FORMAT_PATTERN $DEFAULT_TIME_FORMAT_PATTERN"
    val MS_DATETIME_FORMAT_PATTERN = "$DEFAULT_DATE_FORMAT_PATTERN $MS_TIME_FORMAT_PATTERN"

    val DEFAULT_DATE_FOMATTER = DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT_PATTERN)
    val DEFAULT_TIME_FOMATTER = DateTimeFormatter.ofPattern(DEFAULT_TIME_FORMAT_PATTERN)
    val MS_TIME_FOMATTER = DateTimeFormatter.ofPattern(MS_TIME_FORMAT_PATTERN)
    val DEFAULT_DATETIME_FOMATTER = DateTimeFormatter.ofPattern(DEFAULT_DATETIME_FORMAT_PATTERN)
    val MS_DATETIME_FOMATTER = DateTimeFormatter.ofPattern(MS_DATETIME_FORMAT_PATTERN)

    fun nowStr(): String = LocalDateTime.now().format(DEFAULT_DATETIME_FOMATTER)
    fun currentTimeStr(): String = LocalTime.now().format(DEFAULT_TIME_FOMATTER)
    fun todayStr(): String = LocalDate.now().format(DEFAULT_DATE_FOMATTER)
    fun yestodayStr(): String = LocalDate.now().minusDays(1).format(DEFAULT_DATE_FOMATTER)
    fun tomorrowStr(): String = LocalDate.now().plusDays(1).format(DEFAULT_DATE_FOMATTER)
}