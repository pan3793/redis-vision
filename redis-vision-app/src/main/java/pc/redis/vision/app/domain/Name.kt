package pc.redis.vision.app.domain

sealed class Name {
    abstract val name: String
}

data class Title(
        val servers: List<Server>
) : Name() {
    override val name: String = "服务器列表"
}

data class Server(
        override val name: String,
        val databases: List<Database>
) : Name()

data class Database(
        override val name: String,
        val namespaces: List<Namespace>,
        val keys: List<Key>
) : Name()

data class Namespace(
        override val name: String,
        val subNamespaces: List<Namespace>,
        val keys: List<Key>
) : Name()

data class Key(
        override val name: String
) : Name()