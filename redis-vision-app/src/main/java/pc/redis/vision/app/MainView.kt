package pc.redis.vision.app

import javafx.geometry.Orientation
import javafx.scene.control.TextArea
import javafx.scene.control.TreeItem
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import pc.redis.vision.app.domain.*
import pc.redis.vision.app.util.DateUtil
import pc.redis.vision.app.util.TestData
import tornadofx.*

class MainView : View(title = "RedisVision") {

    private lateinit var logTextArea: TextArea

    override val root = borderpane {
        center {
            splitpane {
                setDividerPositions(0.3)
                treeview<Name> {
                    root = TreeItem(TestData.tree)
                    cellFormat {
                        text = it.name
                    }
                    populate { parent ->
                        when (val value = parent.value) {
                            is Title -> {
                                value.servers
                            }
                            is Server -> {
                                value.databases
                            }
                            is Database -> {
                                value.namespaces + value.keys
                            }
                            is Namespace -> {
                                value.subNamespaces + value.keys
                            }
                            is Key -> {
                                emptyList()
                            }
                        }
                    }
                }
                splitpane(Orientation.VERTICAL) {
                    setDividerPositions(0.8)
                    tabpane {
                        tab("Screen 1", VBox()) {
                            button("Button 1")
                            button("Button 2").setOnAction { logInfo("clicked Button 2") }
                        }
                        tab("Screen 2", HBox()) {
                            button("Button 3")
                            button("Button 4")
                        }
                    }
                    logTextArea = textarea("${DateUtil.nowStr()} Redis Vision start!\n") {
                        isEditable = false
                    }
                }
            }
        }
    }

    fun logPrint(message: String) {
        this.logTextArea.appendText(message)
    }

    fun logPrintln(message: String) {
        this.logTextArea.appendText(message)
        this.logTextArea.appendText("\n")
    }

    fun logInfo(message: String) {
        this.logTextArea.appendText("${DateUtil.nowStr()} ")
        this.logTextArea.appendText(message)
        this.logTextArea.appendText("\n")
    }
}