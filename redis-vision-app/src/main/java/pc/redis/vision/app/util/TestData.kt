package pc.redis.vision.app.util

import pc.redis.vision.app.domain.*

object TestData {
    val tree = Title(
            servers = listOf(Server(
                    name = "redis001",
                    databases = listOf(
                            Database(
                                    name = "db1",
                                    namespaces = listOf(
                                            Namespace(
                                                    name = "n1",
                                                    subNamespaces = listOf(
                                                            Namespace(
                                                                    name = "n1.1",
                                                                    subNamespaces = emptyList(),
                                                                    keys = listOf(Key("1"))
                                                            )
                                                    ),
                                                    keys = emptyList()
                                            )
                                    ),
                                    keys = listOf(Key("1"))
                            )
                    )
            )))
}