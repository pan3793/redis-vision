package pc.redis.vision.app

import de.codecentric.centerdevice.MenuToolkit
import javafx.scene.control.Menu
import javafx.scene.control.MenuItem
import javafx.scene.control.SeparatorMenuItem
import javafx.scene.input.KeyCombination
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler
import tornadofx.*


class Application : WorkspaceApp(MainView::class) {
    companion object {
        init {
            SLF4JBridgeHandler.removeHandlersForRootLogger()
            SLF4JBridgeHandler.install()
        }

        private val log: Logger = LoggerFactory.getLogger(Application::class.java)
        private const val appName = "RedisVision"

        @JvmStatic
        fun main(args: Array<String>) {
            log.debug("{} startup with args: {}", appName, args)
            launch<Application>(args)
            log.debug("{} exit", appName)
        }
    }

    override fun onBeforeShow(view: UIComponent) {
        super.onBeforeShow(view)

        this.createMenuBar(view)
        this.configWindow(view)
    }

    private fun createMenuBar(view: UIComponent) {
        this.createAppMenu()

        view.menubar {
            useSystemMenuBarProperty().set(true)
            menu("Operate") {
                menu("New") {
                    item("Connection").action { log.info("click [Connection]") }
                }
                item("Import").action { log.info("click [Import]") }
                item("Export").action { log.info("click [Export]") }
            }
            menu("Help") {
                item("Help").action { log.info("click [Help]") }
            }
        }
    }

    private fun configWindow(view: UIComponent) {
        view.workspace.setWindowMinSize(800, 600)
        view.primaryStage.centerOnScreen()
    }

    private fun createAppMenu() {
        // only for macOS
        MenuToolkit.toolkit()?.apply {
            Menu(appName, null,
                    createAboutMenuItem(appName),
                    SeparatorMenuItem(),
                    MenuItem("Preferences...").apply {
                        acceleratorProperty().set(KeyCombination.valueOf("Shortcut+,"))
                        setOnAction { log.info("click [Preferences]") }
                    },
                    SeparatorMenuItem(),
                    createHideMenuItem(appName),
                    createHideOthersMenuItem(),
                    createUnhideAllMenuItem(),
                    SeparatorMenuItem(),
                    createQuitMenuItem(appName)
            ).apply {
                setApplicationMenu(this)
            }
        }
    }
}
